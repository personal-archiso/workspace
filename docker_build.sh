#!/bin/bash

docker build --tag 'archiso_build_image' --ulimit=nofile=1024:1048576 $(dirname $0)/.devcontainer
