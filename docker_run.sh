#!/bin/bash

docker run --privileged --ulimit=nofile=1024:1048576 \
    --mount source=$(dirname $0),target=/archiso,type=bind \
    --workdir "/archiso" \
    --interactive --tty 'archiso_build_image' \
    bash
